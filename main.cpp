#include <cstdio>
#include <cstdlib>
#include <cstdint>

#include <iostream>

class SourceCode {
    public:
    SourceCode(char *buf, size_t len) {
        for (size_t i = 0; i < len; i++) {
            if (buf[i] == ' '  ||
                buf[i] == '\t' ||
                buf[i] == '\n' ) continue;

            this->buflen++;
        }

        this->buffer = new char[len];

        for (size_t i = 0, x = 0; i < len; i++) {
            if (buf[i] == ' '  ||
                buf[i] == '\t' ||
                buf[i] == '\n' ) continue;

            this->buffer[x++] = buf[i];
        }
        
        this->pos = 0;
    }

    ~SourceCode() {
        delete[] this->buffer;
    }
    
    void setIP(size_t pos_) {
        this->pos = pos_;
    }
    size_t getIP(void) {
        return this->pos;
    }
    char nextInstruction() {
        // if position out of bounds, return exit (X) instruction
        if (this->pos >= this->buflen) return 'X';

        // otherwise return next instruction, and increment ip
        return this->buffer[this->pos++];
    }

    bool inBounds() {
        return this->pos < this->buflen;
    }
    
    private:
    size_t pos = 0;
    char *buffer = NULL;
    size_t buflen = 0;
};

template <typename t>
class Stack {
    public:
    void push(t val) {
        if (this->alloced < this->len * sizeof(t) + sizeof(t)) {
            this->alloced += 10*sizeof(t);
            this->values = static_cast<t*>(std::realloc(this->values, this->alloced));
        }
        this->values[this->len++] = val;
    }

    t pop() {
        if (this->len == 0) return 0;
        if (this->alloced - 10*sizeof(t) > this->len*sizeof(t)) {
            this->alloced -= 10*sizeof(t);
            this->values = static_cast<t*>(std::realloc(this->values, this->alloced));
        }
        return this->values[--this->len];
    }

    t top() {
        return this->len == 0 ? 0 : this->values[this->len-1];
    }

    void print(std::ostream &os) {
        if (this->values == NULL) return;
        for (size_t i = 0; i < this->len && i < this->len - 1; i++) {
            os << this->values[i] << "\n";
        }
        if (this->len > 0) os << values[this->len - 1];
    }

    private:
    t* values = NULL;
    size_t len = 0;
    size_t alloced = 0;
};

template<typename t>
std::ostream& operator<<(std::ostream &os, Stack<t> &a) {
    a.print(os);
    return os;
}
    

//#define DEBUG

#ifdef DEBUG
#define dbg(x) std::cerr << x << std::endl;
#else
#define dbg(x)
#endif

static uint32_t run(SourceCode *src) {
    // operand stack
    Stack<uint32_t> stack;
    
    while (true) {
        char instr = src->nextInstruction();
        dbg(stack);

        switch (instr) {
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            stack.push(static_cast<uint32_t>(instr - 48));
            dbg("push " << instr);
            break;
        case '+': {
            uint32_t b = stack.pop();
            uint32_t a = stack.pop();

            dbg("add (" << a << " + " << b << " = " << a+b << ")");
            stack.push(a+b);
            break;
        }
        case '-': {
            uint32_t b = stack.pop();
            uint32_t a = stack.pop();

            dbg("sub (" << a << " - " << b << " = " << a-b << ")");
            stack.push(a-b);
            break;
        }
        case '*': {
            uint32_t b = stack.pop();
            uint32_t a = stack.pop();

            dbg("mult (" << a << " * " << b << " = " << a*b << ")");
            stack.push(a*b);
            break;
        }
        case '/': {
            uint32_t b = stack.pop();
            uint32_t a = stack.pop();

            dbg("div (" << a << " / " << b << " = " << a/b << ")");
            stack.push(a/b);
            break;
        }
        case '%': {
            uint32_t b = stack.pop();
            uint32_t a = stack.pop();

            dbg("mod (" << a << " % " << b << " = " << a%b << ")");
            stack.push(a % b);
            break;
        }
        case 'o': {
            uint32_t c = stack.pop();
            dbg("out '" << c << '\'');
            std::putchar(static_cast<int>(c));
            break;
        }
        case 'J': {
            uint32_t inst = stack.pop();
            dbg("jmp " << inst);
            src->setIP(inst);
            break;
        }
        case 'j': {
            uint32_t inst = stack.pop();
            uint32_t val = stack.pop();
            if (val != 0) {
                dbg("jnz (yes) " << inst);
                src->setIP(inst);
            } else {
                dbg("jnz (no) " << inst);
            }
            break;
        }
        case 'd': {
            uint32_t a = stack.pop();
            dbg("dup [" << a << ']');
            stack.push(a);
            stack.push(a);
            break;
        }
        case 'D': {
            uint32_t b = stack.pop();
            uint32_t a = stack.pop();
            dbg("dup2 [" << a << ']');
            stack.push(a);
            stack.push(b);
            stack.push(a);
            break;
        }
        case 'i': {
            uint32_t a = stack.pop();
            dbg("inc [" << a << " => " << a+1 << ']');
            stack.push(a+1);
            break;
        }
        case 'x': {
            uint32_t b = stack.pop();
            uint32_t a = stack.pop();
            dbg("xchg [" << a << " <=> " << b << ']');
            stack.push(b);
            stack.push(a);
            break;
        }
        case 'y': {
            uint32_t b = stack.pop();
            uint32_t c = stack.pop();
            uint32_t a = stack.pop();
            dbg("xchg2 [" << a << " <=> " << b << ']');
            stack.push(b);
            stack.push(c);
            stack.push(a);
            break;
        }
        case 'p': {
            stack.pop();
            dbg("pop");
            break;
        }
        case 'I':
            stack.push(src->getIP() - 1);
            break;
        case '|': 
            while (src->inBounds() && src->nextInstruction() != '|');
            break;
        case 'l': {
            uint32_t v = stack.pop();
            uint32_t old_ip = src->getIP();
            
            src->setIP(0);
            for (int i = -1; src->inBounds() && i < static_cast<int>(v);)
                if (src->nextInstruction() == 'L') i++;

            stack.push(src->getIP());
            src->setIP(old_ip);
            break;
        }
        case 'X':
            dbg("exit");
            return stack.pop();
        case '_':
        case 'L':
            // do nothing
            break;
        default:
            dbg("invalid instruction '" << instr << '\'');
            break;
        }
    }
}

int main(int argc, char **argv) {
    // get argument
    if (argc < 2) {
        std::cerr << "Not enough arguments" << std::endl;
        std::exit(1);
    }

    if (argc > 2) {
        std::cerr << "Too many arguments" << std::endl;
        std::exit(1);
    }
    
    const char *const filename = argv[1];
    
    // read file
    FILE *file = std::fopen(filename, "rb");

    if (file == NULL) {
        std::perror("fopen");
        std::exit(1);
    }

    // load source code

    // go to end of file
    if (fseek(file, 0l, SEEK_END) < 0) {
        std::perror("fseek");
        std::exit(1);
    }

    // get file length
    ssize_t size = ftell(file);
    if (size < 0) {
        std::perror("ftell");
        std::exit(1);
    }

    char *buf = new char[size];

    // rewind stream
    rewind(file);

    // read to buffer
    fread(buf, sizeof(char), static_cast<size_t>(size), file);

    // interpret buffer
    auto sc = SourceCode(buf, static_cast<size_t>(size));
    delete[] buf;
    uint32_t ret_code = run(&sc);

    // exit success
    return static_cast<int>(ret_code);
}
