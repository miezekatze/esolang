# Esostack - A stack based esoteric programming Language

## Functionality

Every character is seen as an instruction (unmapped characters are ignored, but the goal is to make everything an instruction)

This language operates on a single operand stack of unsigned 32-bit integers and _nothing else_. It does not support functions, if, while, etc, only conditional jumps.

If the stack does not contain any value (size 0) all read instructions get 0 as values

## Instructions

(Arguments are sorted from top to bottom -> first argument = first popped value)

| Char | Name   | Arguments      | Description                                                         |
| ---- | ------ | -------------- | ------------------------------------------------------------------- |
| X    | exit   | _val_          | exits the program with _val_ as exit code                           |
| 0-9  | pushi  | _              | pushes the number (0 - 9) onto the stack                            |
| +    | add    | _b_, _a_       | adds _a_ and _b_ and pushes the result                              |
| -    | sub    | _b_, _a_       | subtracts _b_ from _a_ and pushes the result                        |
| *    | mult   | _b_, _a_       | multiplies _a_ and _b_ and pushes the result                        |
| /    | div    | _b_, _a_       | divides _a_ by _b_ and pushes the result                            |
| %    | mod    | _b_, _a_       | pushes _a_ % _b_ back on the stack                                  |
| o    | out    | _c_            | outputs _c_ as an ASCII character (chopped down to 1 byte)          |
| J    | jmp    | _instr_        | jumps to  instruction _instr_                                       |
| j    | jnz    | _instr_, _val_ | jumps to _instr_ if _val_ is not zero                               |
| d    | dup    | _val_          | pushes _val_ 2 times onto the stack                                 |
| D    | dup2   | _a_, _b_       | pushes _a_, _b_, _a_ back                                           |
| i    | inc    | _val_          | increments _val_ and pushes it back                                 |
| x    | xchg   | _b_,  _a_      | exchanges top values => pushes _b_, then _a_                        |
| y    | xchg2  | _b_, _c_, _a_  | exchanges first and second values => pushes _b_, then _c_, than _a_ |
| _\__ | nop    | _\__           | does nothing                                                        |
| _p_  | pop    | _a_            | pops value _a_ from stack                                           |
| I    | puship | _\__           | pushes the current instruction pointer onto the stackr              |
| \|   | jnp    | _\__           | jumps to the next pipe (\|) character (can be used as comment)      |
| L    | label  | _\__           | a label (nop)                                                       |
| l    | jmpl   | _i_            | jumpes to _ith_ label                                               |
