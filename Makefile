WARNING_FLAGS=-pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused -Wfloat-conversion

CXXFLAGS=-O3 -ggdb -std=c++20 ${WARNING_FLAGS}
LDFLAGS=

HEADERS=$(shell find -name '*.hpp')
FILES=$(shell find -name '*.cpp')
OUTFILE=main

CC=g++
LIB_PREFIX=/usr/lib/

# If the first argument is "run"...
ifeq (run,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

all: main

.PHONY main:
main: ${FILES} ${HEADERS}
	${CC} ${CXXFLAGS} ${LDFLAGS} ${FILES} -o ${OUTFILE}

.PHONY: clean
clean:
	@rm ${OUTFILE}
	@echo "Cleaned up"

.PHONY run:
run: all
	./${OUTFILE} $(RUN_ARGS)
